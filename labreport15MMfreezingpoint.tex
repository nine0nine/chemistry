\documentclass[12pt,oneside,letterpaper]{article}
\usepackage{rotating}
\usepackage{rotfloat}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}
\usepackage{mhchem}
\sisetup{per-mode  = symbol}%
\sisetup{qualifier-mode = subscript}%
\usepackage{hyperref}
\usepackage{tkz-berge}
\usepackage{color}
\usepackage{xfrac}
\usepackage{graphicx}
\usepackage[style = chem-acs]{biblatex}
\addbibresource{references.bib}
\setboolean{@twoside}{false}
% New definition of square root:
% it renames \sqrt as \oldsqrt
\usepackage{letltxmacro}
\makeatletter
\let\oldr@@t\r@@t
\def\r@@t#1#2{%
\setbox0=\hbox{$\oldr@@t#1{#2\,}$}\dimen0=\ht0
\advance\dimen0-0.2\ht0
\setbox2=\hbox{\vrule height\ht0 depth -\dimen0}%
{\box0\lower0.4pt\box2}}
\LetLtxMacro{\oldsqrt}{\sqrt}
\renewcommand*{\sqrt}[2][\ ]{\oldsqrt[#1]{#2} }
\makeatother
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\everymath{\displaystyle}
\title{\textit{Freezing Point Depression and Molar Mass}}
\author{Caspian Baskin}

\begin{document}

\maketitle
\begin{description}
    \item[1.] \hfill \\
        It is not quite as close as I would have hoped, but this may be due to the fact that I did not set the sample rate properly on the Labquest unit. The average molar mass I got was \SI{164.6}{\gram\per\mole}, and this is closest (out of the provided possibilities) to Camphor. This is in fact what the IR spectra confirmed. The peak with the greatest amplitude on both graphs is around \SI{1750}{\per\centi\meter}, and corresponds to the bond stretch between the \ce{C=O} atoms. The next largest peak has a couple of sub peaks and falls between \SI{2800}{\per\centi\meter} and \SI{3000}{\per\centi\meter}. This one is likely a result of the stretching of \ce{C-H} bonds in the compounds cycloalkane body. The last notable area I see is a number of smaller peaks between \SI{1350}{\per\centi\meter} and \SI{1450}{\per\centi\meter}. This area is also a good identifier for the alkane group as it corresponds to the bending and rocking of \ce{C-H} bonds. All of the above mentioned peaks line up remarkably well between the two graphs even down to smaller peaks that are part of the larger peak.
        \begin{figure} [H]
            \centering
            \includegraphics[scale=0.45]{IR_spectra_camphor.pdf}
            \includegraphics[scale=0.45]{camphor.pdf}
        \end{figure}
    \item[2.] \hfill \\
        Well this question is tricky because I am unsure of what a common margin of error is in this type of calculation. It would seem to me that the difference in molar mass is substantial however if I calculate the actual number of moles based on the weight of the camphor its only off by about 8\%. In this experiment there was a significant amount of naphthalene vaporizing and condensing on the sides of the test tube, and because this substance sublimes at room temperature, it is possible that a somewhat significant portion of the naphthalene escaped the tube as a gas. In addition to losses some of the naphthalene on the sides of the tube did not melt with subsequent reheatings. I think the other major source of error here, and I may be overly hopeful of it's effect based on my desire for the numbers to work out well, is the clumping of solute molecules in the solution. If for some reason about 8\% of the camphor molecules were still paired up and not totally dissolved in the solution, they would act as a single molecule in the freezing point depression and explain why the calculated molar mass is off by almost \SI{10}{\gram}.
    \item[3.] \hfill \\
        \begin{align*}
            \Delta \text{T} &= \text{K}_{f} m (i) \\
            \text{K}_{f} \ce{H2O} &= \SI{1.86}{\degreeCelsius\kilo\gram\per\mole} \\
            \intertext{with a theoretical Van't Hoff factor of two, \ce{NaCl} would be,}
            \iota &= \SI{-1.86}{\degreeCelsius\kilo\gram\per\mole} \left(\SI{.10}{\mole\per\kilo\gram}\right) (2) \  \ [\Delta\text{T}] \\
            \iota &= \SI{-.372}{\degreeCelsius} \     \ [\Delta\text{T}] \\
            \intertext{and for \ce{BaCl2} a Van't Hoff of 3,}
            \iota &= \SI{-1.86}{\degreeCelsius\kilo\gram\per\mole} \left(\SI{.10}{\mole\per\kilo\gram}\right) (3) \  \ [\Delta\text{T}] \\
            \iota &= \SI{-0.558}{\degreeCelsius} \     \ [\Delta\text{T}] 
        \end{align*}
        So we can see the values are definitely different. The \ce{NaCl} is off by almost 6\% and the \ce{BaCl2} is off by 18\%. I think the best explanation for this is that our \(i\) values are off for these solutions, and that not all of the ions have dissociated or are clumping together and acting as a single molecule. I can calculate revised Van't Hoff factors and hopefully get more predictable results for future experiments performed with the same substances and procedures.
        \begin{align*}
            \SI{-.348}{\degreeCelsius} &= \SI{-1.86}{\degreeCelsius\kilo\gram\per\mole} \left(\SI{.10}{\mole\per\kilo\gram}\right) \iota \    \ [i] \\
                \iota &= 1.87 \\
                \intertext{for \ce{NaCl}, and for \ce{BaCl2} we get,}
                \SI{-.470}{\degreeCelsius} &= \SI{-1.86}{\degreeCelsius\kilo\gram\per\mole} \left(\SI{.10}{\mole\per\kilo\gram}\right) \iota \    \ [i] \\
                    \iota &= 2.53
                \end{align*}
            \item[4.] \hfill \\
                \begin{gather*}
                    \Delta \text{T} = \text{K}_{f} m \\
                    \ce{C10H8} = \SI{128.173}{\gram\per\mole} \\
                    \frac{\SI{.556}{\gram}}{\SI{128.173}{\gram\per\mole}} = \SI{0.00433788707450087}{\mole\of{\ce{C10H8}}} \\
                    \frac{\SI{0.00433788707450087}{\mole\of{\ce{C10H8}}}}{\SI{15.167}{\gram\of{\ce{C10H16O}}}}\left[\frac{\SI{1000}{\gram}}{\SI{1}{\kilo\gram}}\right] = \SI{0.286008246489146}{\mole\of{\ce{C10H8}}\per\kilo\gram\of{\ce{C10H16O}}} \\
                    \SI{10.78}{\degreeCelsius} = \iota \left(\SI{0.286008246489146}{\mole\of{\ce{C10H8}}\per\kilo\gram\of{\ce{C10H16O}}}\right) \ \ [\text{K}_{f}] \\
                    \iota = \SI{37.6912209082373}{\degreeCelsius\kilo\gram\per\mole} \ \ [\text{K}_{f}] \\
                    \text{K}_{f} \approx \SI{37.69}{\degreeCelsius\kilo\gram\per\mole}
                \end{gather*}
            \item[5.] \hfill \\
                \begin{align*}
                \Delta \text{T} &= \text{K}_{f} m \\
                \SI{10}{\degreeCelsius} &= \SI{1.86}{\degreeCelsius\kilo\gram\per\mole}\left(\frac{\iota}{\SI{1}{\kilo\gram}}\right) \ \ [\si{\mole\of{\text{substance}}}] \\
                \iota &= \SI{5.37634408602151}{\mole} \\
                \intertext{now with molar masses of...}
                \ce{CH3OH} &= \SI{32.0416}{\gram\per\mole} \\
                \ce{C2H4(OH)2} &= \SI{62.0674}{\gram\per\mole} \\
                \intertext{so we just need to find the mass of each,}
                \left[\SI{5.37634408602151}{\mole}\right]\left[\SI{32.0416}{\gram\per\mole}\right] &= \SI{172.266666666667}{\gram\of{\ce{CH3OH}}} \\
                &\approx \SI{172.27}{\gram\of{\ce{CH3OH}}} \\
                \intertext{and,}
                \left[\SI{5.37634408602151}{\mole}\right]\left[\SI{62.0674}{\gram\per\mole}\right] &= \SI{333.695698924732}{\gram\of{\ce{C2H4(OH)2}}} \\
                &\approx \SI{333.70}{\gram\of{\ce{C2H4(OH)2}}}
            \end{align*}
            I would say the major advantage that ethylene glycol has over methyl alcohol is its stability. Methyl alcohol evaporates quickly meaning each time the cooling system is opened you would lose some fraction of moles to the air. In addition methyl alcohol is quite flammable and the vapors could combust explosively, this could lead to many unfortunate situations when it is wrapped around and ``internal combustion'' engine, if you catch my drift. Methanol is also a polar molecule and slightly acidic, which would accelerate electrolysis in a vehicle cooling system, methanol itself is highly corrosive to aluminum converting its oxidised surface into salt that are soluble in methanol leaving a clean surface to again be oxidised. Ethylene glycol by comparison is far less corrosive and acidic has a more stable vapor pressure and has by itself a higher boiling point than water providing some cushion in the event that some water evaporates from the system.



\printbibliography
\end{document}

