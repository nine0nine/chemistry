\documentclass[12pt,oneside,letterpaper]{article}
\usepackage{rotating}
\usepackage{rotfloat}
\usepackage{mathtools}
\usepackage{amssymb}
\usepackage{siunitx}
\usepackage{mhchem}
\sisetup{per-mode  = reciprocal}%
\sisetup{qualifier-mode = subscript}%
\usepackage{hyperref}
\usepackage{tkz-berge}
\usepackage{color}
\usepackage{xfrac}
\usepackage{graphicx}
\usepackage{chemfig}
\usepackage[style = chem-acs]{biblatex}
\addbibresource{references.bib}
\setboolean{@twoside}{false}
% New definition of square root:
% it renames \sqrt as \oldsqrt
\usepackage{letltxmacro}
\makeatletter
\let\oldr@@t\r@@t
\def\r@@t#1#2{%
\setbox0=\hbox{$\oldr@@t#1{#2\,}$}\dimen0=\ht0
\advance\dimen0-0.2\ht0
\setbox2=\hbox{\vrule height\ht0 depth -\dimen0}%
{\box0\lower0.4pt\box2}}
\LetLtxMacro{\oldsqrt}{\sqrt}
\renewcommand*{\sqrt}[2][\ ]{\oldsqrt[#1]{#2} }
\makeatother
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\everymath{\displaystyle}
\title{\textit{Systems in Chemical Equilibrium}}
\author{Caspian Baskin}

\begin{document}

\maketitle
\begin{description}
    \item[Experiment A] \hfill \\
        In this experiment we combined around \SI{5}{\milli\liter} of distilled water and a few drops of methyl violet (probably \ce{C24H28N3Cl}). In solution methyl violet partially dissociates in the equilibrium equation,
        \begin{gather}
            \ce{C24H28N3Cl(aq) <--> H+(aq) + C24H27N3Cl-(aq)}.
        \end{gather}
        The color of the ion is violet while the color of the compound is yellow. By manipulating the hydronium ion concentration in the solution we can affect a color change of our solution. It seems (according to wikipedia) that the pH range for the color change is somewhere between pH values of 0-1.6. In our experiment the first reagent we added was a 6M solution of hydrochloric acid (\ce{HCl}). This pretty rapidly changed the color from a deep violet to yellow as we effected the equilibrium of the methyl violet by massively increasing the hydronium ion concentration, and driving the reaction towards the reactant side effectively reducing the violet colored ion's concentration to 0. In order to change the color back we needed to raise the pH, or raise the hydroxide (\ce{OH-}) concentrations, This would disrupt the equilibrium reaction of self ionized water molecules,
        \begin{gather}
            \ce{H2O(l) <--> H+(aq) + OH-(aq)},
        \end{gather}
        and since our hydronium concentration is already extremely high adding a similar amount of \ce{OH-} ions would drive \emph{this} reaction towards reactants neutralizing the majority of free hydronium ions and allowing the methyl violet reaction to produce the violet ion once again. The interesting thing is that although adding a drop of 6M \ce{NaOH} to the solution changed the color back to a violet hue, it was much lighter than the original solution and soon became completely clear. From what I can tell this is due to the strongly charged \ce{OH-} ions breaking up the structure of the methyl violet into triphenylmethanol which is colorless.
    \item[Experiment B] \hfill \\
        \begin{description}
            \item[a.] \hfill \\
                For the reaction 
                \begin{gather}
                    \ce{PbCl2(s) <-->Pb^{2+}(aq) + 2Cl-(aq)}
                \end{gather}
                the equilibrium constant \(K_{c}\), or in this case the \emph{solubility product constant} \(K_{sp}\) must be met by \([\ce{Pb^{2+}}]\cdot[\ce{Cl}]^{2}\), in order for the solid to precipitate.
            \item[b.] \hfill \\
                When the solution with solid present was submerged in the boiling water it dissolved (or dissociated) into the constituent ions. This implies that the application of heat raised the \(K_{sp}\) value which would in turn disrupt the equilibrium requiring higher concentrations of ions to form the precipitate. This must then mean that \ce{PbCl2} has a negative \(\Delta H\) value. Practically, this means that when \ce{PbCl2} is formed energy is released by the ionic, elemental atoms as they enter into a lower energy state, as a result more energy input is required to dissolve the high energy crystal lattice.
            \item[c.] \hfill \\
                Since \(K_{sp}\) is dependant on concentration, in this case molarity, which is measured by the number of moles of a substance per unit liter, decreasing the concentration will change the reaction quotient and disrupt the equilibrium. In this case adding additional \ce{H2O} to the solution decreases the concentration of \([\ce{Pb^{2+}}]\) and \([\ce{Cl-}]\). Since this is a solubility equilibrium of the form 
                \begin{gather*}
                    \ce{A(s) <--> B(aq) + C(aq)}
                \end{gather*}
                the mass action equation takes the form  
                \begin{gather*}
                    K_{sp} = [\ce{B}][\ce{C}].
                \end{gather*}
                By lowering the concentration of the aqueous ions we can bring the value of the reaction quotient below that of \(K_{sp}\) causing the solid to dissolve and breaking the equilibrium.
            \item[d.] \hfill \\
                \begin{align*}
                    \text{Volume of \SI{0.30}{M} \,\ce{Pb^{+2}}}=&\SI{5.0}{\milli\liter} \\[2mm]
                    \text{Volume of \SI{0.30}{M} \, \ce{Cl-}}=&\SI{2.0}{\milli\liter} \\[2mm]
                    \text{Volume of \ce{H2O}}=&\SI{7.0}{\milli\liter} \\[2mm]
                    \shortintertext{now we can find the concentration of ions in the solution.} \\
                    \frac{\SI{0.3}{\mole\of{\ce{Pb^{2+}}}}}{\SI{1}{\liter}}\left(\frac{\SI{0.005}{\liter}}{\SI{14}{\liter}}\right) =& \SI{0.000107142857142857}{{M}\of{\ce{Pb^{2+}}}} \\[2mm]
                    \approx&\SI{1.1e-4}{{M}\of{\ce{Pb^{2+}}}} \\[2mm]
                    \frac{\SI{0.30}{\mole\of{\ce{Cl-}}}}{\SI{1}{\liter}}\left(\frac{\SI{.002}{\liter}}{\SI{14}{\liter}}\right)=&\SI{0.0000428571428571429}{{M}\of{\ce{Cl-}}} \\[2mm]
                    \approx&\SI{4.3e-5}{{M}\of{\ce{Cl-}}} \\[2mm]
                    \shortintertext{So now we can calculate \(K_{sp}\)}
                    \ce{PbCl2(s) <-->& Pb^{2+}(aq) + Cl-(aq)} \\[2mm]
                    K_{sp}=& [\ce{Pb^{2+}}][\ce{Cl-}]^{2} \\[2mm]
                    K_{sp}=& \left(\num{0.000107142857142857}\right)\left(\num{0.0000428571428571429}\right)^{2} \\[2mm]
                    K_{sp}=& \num{7.87172011661810e-14} \\[2mm]
                    K_{sp}\approx& \num{7.9e-14}
                \end{align*}
                I'm pretty sure that this number is way off and I can only guess that this is due to cavalier water additions at the end of the procedure, it is possible that the salt would have dissolved in less water if given a little more time and turbulence.
        \end{description}
        \newpage
    \item[Experiment C] \hfill \\
    \begin{description}
        \item[a.] \hfill \\
            The solution of \ce{Co(H2O)6^{2+}} and water first had concentrated \ce{HCL} added, which allowed for the equilibrium reaction,
            \begin{gather}
                \ce{Co(H2O)6^{2+}(aq) + 4Cl-(aq) <--> CoCl4^{2-}(aq) + 6H2O}
            \end{gather}
            which produced a lot of \ce{CoCl4^{2-}} due the overwhelming concentration of \ce{Cl-} ions. By changing the number and type of ligands the wavelength of light absorbed by the complex is changed and the visible color of the solution changes. In this case the addition of \ce{HCL} changed the solution from a light pink to a deep blue color. On the addition of distilled water \ce{H2O} we, again, disrupted the equilibrium by increasing the concentration on the product side. This unbalanced the equilibrium, greatly increasing the reaction quotient \(Q_{c}\). As a result the equilibrium was driven to the reactant side, the \ce{Cl-} ligands were again replaced by \ce{H2O} increasing the concentration of \ce{Co(H2O)6^{2+}}.
        \item[b.] \hfill \\
            When the diluted solution was heated it changed from pink to a light blue. This leads to the conclusion that \(\Delta H\) of [my] reaction 4 is positive, and the reaction is endothermic, which necessarily means that the reverse reaction is exothermic. An exothermic reaction releases heat as lower energy bonds and electron configurations are formed for the component atoms. This means that the fraction of molecules with the amount of energy needed to separate goes down as the heat is released, and as a result, if a large amount of additional energy is introduced, the amount of molecules with sufficient energy to separate goes up and the reaction slows or even reverses. So, since the thermodynamics of this particular reaction favors the reactants side of the equilibrium, adding excess heat to the reaction raises the \(K_{c}\) value for the equilibrium and drives the process to products. So when heat was applied to the solution the equilibrium constant was changed to a value that allowed for more \ce{CoCl4^{2-}} to form. The \(\Delta H\) for [my] reaction 4 should be a positive value.
    \end{description}
        \item[Experiment D] \hfill \\
            \begin{description}
                \item[1.] \hfill \\
                    The addition of one drop of \ce{NaOH} to the \ce{Zn(NO3)2} solution caused the liquid to become cloudy with fine precipitate. Setting up the equillibrium reaction,
                    \begin{gather}
                        \ce{Zn(OH)2(s) <--> ZN^{2+}(aq) + 2OH-(aq)}
                    \end{gather}
                \item[2.] \hfill \\
                    \begin{description}
                        \item[a.] \hfill \\
                            On the direction of the lab write up I used the smallest test tubes we had, which was apparently a mistake. I added a fair amount of \ce{HCl} to the tube with some initial dissolving of the precipitate, but it seemed to slow pretty quickly. After realizing that the experiment wasn't going as it should we shook up the tube and all the precipitate dissolved. So it would seem that the \ce{HCl} increased the solubility of the \ce{Zn(OH)2} by changing the reaction quotient of water's self ionization equilibrium by adding excess \ce{H+} ions. As a result \ce{OH-} ions were greatly decreased by the formation of \ce{H2O}, which in turn forced the concentration of \ce{Zn^{2+}} ions to increase which drove the \ce{Zn(OH)2} to dissociate.
                        \item[b.] \hfill \\
                            As more \ce{OH-} ion was added the precipitate did not continue to form. I added a fair amount of \ce{NaOH} to the solution but due to the restricted size of the tube the solution was not mixing. In fact it appeared as though the precipitate was separating the remaining \ce{Zn(NO3)2} from the newly added \ce{NaOH} solutions. After shaking the tube some of the solid dissolved. The heavy increase of \ce{OH-} ions caused a new reaction to start 
                            \begin{gather}
                                \ce{Zn^{2+}(aq) + 4OH-(aq) <--> Zn(OH)4^{2-}(aq)}.
                            \end{gather}
                            With the greater concentration of \ce{OH-} ions this reactions \(K_{c}\) is pretty high at \num{3e15} which greatly reduces the concentration of \ce{Zn^{2+}} ions, again driving the first reaction towards products.
                        \item[c.] \hfill \\
                            Although I had a similar problem with the addition of \ce{NH3} (it didn't mix well), after shaking this one also became clear. I believe this is due to the creation of excess \ce{OH-} ions through the deprotonation of \ce{H2O}. This increase in \ce{OH-} had a greater effect than direct addition of \ce{OH-} had, but this may be due to the fact that I added a lot of ammonia due to the solid not dissolving as I had expected it to because of the tiny test tubes. After shaking, the solid completely dissolved.
                    \end{description}
                \item[3.] \hfill \\
                    The reaction to the initial drop of \ce{NaOH} was the same for \ce{Mg(NO3)2} as it was for \ce{Zn(No3)2}. Some precipitate formed causing the solution to become cloudy.
                    \begin{description}
                        \item[a.] \hfill \\
                            The \ce{HCl} pretty quickly dissolved the precipitate to a clear solution. This is due to the reduction of \ce{OH-} ions by the addition of \ce{H+} ions, as well as a second reaction 
                            \begin{gather}
                                \ce{Mg(OH)2(s) + 2HCl(aq) -> MgCl2(aq) + 2H2O}. 
                            \end{gather}
                            This greatly increased the solubility of the \ce{Mg(OH)2}.
                            \item[b.] \hfill \\
                                This one was a bit different, upon addition of more \ce{NaOH} it seemed to create more precipitate and then stopped changing. This obviously decreased the solubility of the \ce{Mg(OH)2}. This is likely due to the equilibrium equation 
                                \begin{gather}
                                    \ce{Mg(OH)2(s) <--> Mg^{2+}(aq) + OH-(aq)}, 
                                \end{gather}
                                becoming unbalanced. With the greatly increased \ce{OH-} the \ce{Mg^{2+}} ion's concentration must get very small to get back to equilibrium, so most, if not all, of it will combine with \ce{OH-} to make \ce{Mg(OH)2}.
                            \item[c.] \hfill \\
                                The \ce{NH3} solution, when added had the same effect as the \ce{OH-}. Solubility decreased.
                        \end{description}
                \end{description}
                \newpage
                \item[Final Questions] \hfill \\
                    I am going to be somewhat brief for these questions since I think I have answered most of this in the previous ``observations'' section, if there is anything new I will try to explain. 
                    \begin{description}
                        \item[Initial reaction] The initial introduction of \ce{NaOH} into the solution sets up the equilibrium reaction causing the \ce{Zn^{2+}} ions and the \ce{OH-} ions to combine and form zinc hydroxide solids.
                        \item[Addition of \ce{H+}] The increase of hydronium ions unbalances the \ce{H2O} self ionisation equilibrium which results in a very small amount of \ce{OH-} ions for the zinc hydroxide reaction which drives it to products (\ce{Zn^{2+} + OH-}).
                        \item[Addition of \ce{OH-}] While it would seem that the \ce{Zn(OH)2} amount should increase with the greater addition of \ce{OH-} ions, it doesn't due to a more favorable reaction with the increased \ce{OH-} concentration to a more soluble complex ion, zincate \ce{Zn(OH)4^{2+}}, which in turn causes the original equilibrium to go to products (\ce{Zn^{2+} + OH-}).
                        \item[Addition of \ce{NH3}] As previously discussed the addition of a weak base such as ammonia has a similar effect as that of the strong base \ce{NaOH}. The hydroxide ion concentration increases which in turn pushes forward the zincate reaction, as well as another zinc based complex \ce{Zn(NH3)4^{2+}}, which is also far more soluble in water due to its reduced charge.
                        \item[Similarities and differences] While the \ce{Mg(OH)2} was similar to the \ce{Zn(OH)2} in the first step, the majority of the rest of the experiment was different. Because the zinc ion is able to form complex ions using both hydroxide and ammonia ligands, it was easily dissolved through the addition of all three chemicals, whereas the magnesium ion is unable to form these bonds. While I'm not sure I think this is likely due to the fact that magnesium is from the third period and therefore unable to expand its valence electrons to include donated electrons from the basic ions. They both, however, dissolved in the acid because the addition of so much \ce{H+} destabilises the water ion equilibrium which, in turn, destabilises the zinc hydroxide equilibrium, forcing the solid to dissociate into its ions in an attempt to reach equilibrium again.
                    \end{description}
\end{description}
\printbibliography
\end{document}

