\documentclass[12pt,oneside,letterpaper]{article}
\usepackage{rotating}
\usepackage{rotfloat}
\usepackage{mathtools}
\usepackage{amssymb}
\usepackage{siunitx}
\usepackage{mhchem}
\sisetup{per-mode  = reciprocal}%
\sisetup{detect-all}%
\sisetup{qualifier-mode = subscript}%
\usepackage{hyperref}
\usepackage{tkz-berge}
\usepackage{color}
\usepackage{xfrac}
\usepackage{graphicx}
\usepackage{chemfig}
\usepackage{enumitem}
\usepackage[style = chem-acs]{biblatex}
\addbibresource{references.bib}
\setboolean{@twoside}{false}
% New definition of square root:
% it renames \sqrt as \oldsqrt
\usepackage{letltxmacro}
\makeatletter
\let\oldr@@t\r@@t
\def\r@@t#1#2{%
\setbox0=\hbox{$\oldr@@t#1{#2\,}$}\dimen0=\ht0
\advance\dimen0-0.2\ht0
\setbox2=\hbox{\vrule height\ht0 depth -\dimen0}%
{\box0\lower0.4pt\box2}}
\LetLtxMacro{\oldsqrt}{\sqrt}
\renewcommand*{\sqrt}[2][\ ]{\oldsqrt[#1]{#2} }
\makeatother
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\everymath{\displaystyle}
\title{\textit{Determination of Equilibrium Constant for a Chemical Reaction}}
\author{Caspian Baskin}

\begin{document}

\maketitle
\begin{description}[style=unboxed]
    \item[Calculation of stock solution in \(\sfrac{\si{\mole}}{\si{\liter}}\)]
        \begin{align*}
            \intertext{since \ce{SCN-} is the limiting reagent in this mixture it should be completely converted to \ce{FeSCN2+}, thus moles of \ce{SCN-} and \ce{FeSCN2+} should be equal.}
            \text{Concentration of \ce{KSCN} used} =& \SI{2.00d-3}{\mole\per\liter} \\[2mm]
            \text{Amount \ce{KSCN} used} =& \SI{2.00d-3}{\liter} \\[2mm]
            \text{Total amount of solution} =& \SI{2.000d-2}{\liter} \\[2mm]
            \intertext{So I need to find the molar concentration of \ce{SCN-} which will give me the molar concentration of \ce{FeSCN2+}.}
        \end{align*}
        \begin{align*}
            \iota=&\frac{\SI{2.00d-3}{\mole\per\liter\of{\ce{KSCN}}}(\SI{.002}{\liter})}{\SI{2.000}{\liter}} &[\si{\mole\per\liter\of{\ce{KSCN}\text{ in solution}}}] \\[2mm]
            \iota=&\SI{2.00d-4}{\mole\per\liter\of{\ce{KSCN}}}
        \end{align*}
        So we have \SI{2.00d-4}{\mole\per\liter\of{\ce{FeSCN^{2+}}}}.
    \item[Calculation of Stock solution sample \#3 molarity] \hfill \\
        \begin{align*}
            \frac{\SI{2.00d-4}{\mole\per\liter\of{\ce{FeSCN^{2+}}}}(\SI{.00101}{\liter})}{\SI{.00351}{\liter}} =& \SI{5.75498575498575d-5}{\mole\per\liter\of{\ce{FeSCN^{2+}}}} \\[2mm]
            \approx& \SI{5.75d-5}{\mole\per\liter\of{\ce{FeSCN^{2+}}}}
        \end{align*}
    \item[Plot of absorbance \emph{vs.} molarity (\ce{FeSCN^{2+}}) and linear fit function ``calibration curve''] \hfill \\
        \begin{figure} [H]
            \centering
            \includegraphics[scale=.8]{equillibrium.pdf}
        \end{figure}
    \item[Calculation of concentration of test mixture 1 using the ``calibration curve'' with Beer's Law] \hfill \\
        \begin{align*}
        \intertext{Beer's Law}
        c=&\text{concentration} \\[2mm]
        k=&\text{proportionality constant} \\[2mm]
        \ell=&\text{path length} \\[2mm]
        A=&kc\ell \\[2mm]
        c=\frac{1}{k \ell} \cdot A \\[2mm]
        \intertext{So from our linear fit function,}
        \num{2.75119646d-4}x \\[2mm]
        \intertext{we have,}
        \SI{2.75119646d-4}{\liter\per\mole}=& \frac{1}{k \ell} \\[2mm]
        \intertext{so applying beers law and our constant we can approximate the concentration of \ce{FeSCN^{2+}} in the solution for mixture 1}
        f=&\SI{2.75119646d-4}{\liter\per\mole} \iota &[\text{absorbance}] \\[2mm]
        f=&\SI{2.5861246724d-5}{\mole\per\liter} &[.094] \\[2mm]
        \approx&\SI{2.59d-5}{\mole\per\liter\of{\ce{FeSCN^{2+}}}} \\[2mm]
        \intertext{now we need to find the number of moles,}
        \SI{2.5861246724d-5}{\mole\per\liter\of{\ce{FeSCN^{2+}}}}\cdot\SI{1.000d-2}{\liter\of{\text{solution}}} \\[2mm]
        =&\SI{2.5861246724d-7}{\mole\of{\ce{FeSCN^{2+}}}} \\[2mm]
        \approx&\SI{2.59d-7}{\mole\of{\ce{FeSCN^{2+}}}}
        \end{align*}
        So now that we have the concentration of products we can calculate the concentration of reactants. In the equation \ce{Fe^{3+} + SCN- <--> FeSCN^{2+}} for every mole of \ce{FeSCN^{2+}} created, 1 mole of \ce{Fe^{3+}} and 1 mole of \ce{SCN-} are lost. Since we know our starting concentrations we can calculate the equilibrium concentrations for mixture 1.
        \begin{align*}
            \intertext{First we'll calculate the number of moles of \ce{Fe(NO3)3},}
            \text{concentration}=&\SI{2.00d-3}{\mole\per\liter} \\[2mm]
            g=&\SI{2.00d-3}{\mole\per\liter}\left(\frac{\SI{1}{\liter}}{\SI{1000}{\milli\liter}}\right)\iota &[\si{\milli\liter\of{\ce{Fe(NO3)3}\text{ solution}}}] \\[2mm]
            g=&\SI{1.00d-5}{\mole\of{\ce{Fe(No3)3}}} &[\SI{5.00}{\milli\liter}]
        \end{align*}
        Now because the initial concentrations of both our reactant solutions are the same we can use the same function to find \ce{KSCN} moles.
        \begin{align*}
            g=&\SI{2.00d-6}{\mole\of{\ce{KSCN}}} &[\SI{1.00}{\milli\liter}]
        \end{align*}
        Now we just subtract the number of moles of \ce{FeSCN^{2+}} from the number of moles of each reactant to get our equilibrium mole counts for mixture 1.
        \begin{align*}
            \SI{1.00d-5}{\mole\of{\ce{Fe(No3)3}}}-\SI{2.5861246724d-7}{\mole}=&\SI{9.74138753276d-6}{\mole\of{\ce{Fe(NO3)3}}} \\[2mm]
            \approx&\SI{9.74d-6}{\mole\of{\ce{Fe(NO3)3}}}
            \intertext{and}
            \SI{2.00d-6}{\mole\of{\ce{KSCN}}}-\SI{2.5861246724d-7}{\mole}=&\SI{1.74138753276d-6}{\mole\of{\ce{KSCN}}} \\[2mm]
            \approx&\SI{1.74d-6}{\mole\of{\ce{KSCN}}}
        \end{align*}
        Now we just determine the molarities based on the number of moles per substance.
        \begin{align*}
            \frac{\SI{1.74138753276d-6}{\mole\of{\ce{KSCN}}}}{\SI{.01}{\liter}}=&\SI{1.74138753276d-4}{\mole\per\liter\of{\ce{KSCN}}} \\[2mm]
            \approx&\SI{1.74d-4}{\mole\per\liter\of{\ce{KSCN}}} \\[2mm]
            \frac{\SI{9.74138753276d-6}{\mole\of{\ce{Fe(NO3)3}}}}{\SI{.01}{\liter}}=&\SI{9.74138753276d-4}{\mole\per\liter\of{\ce{Fe(NO3)3}}} \\[2mm]
            \approx&\SI{9.74d-4}{\mole\per\liter\of{\ce{Fe(NO3)3}}} \\[2mm]
            \frac{\SI{2.5861246724d-7}{\mole\of{\ce{FeSCN^{2+}}}}}{\SI{.01}{\liter}}=&\SI{2.5861246724e-5}{\mole\of{\ce{FeSCN^{2+}}}} \\[2mm]
            \approx&\SI{2.59e-5}{\mole\of{\ce{FeSCN^{2+}}}}
        \end{align*}
        And now to calculate \(K_{c}\).
        \begin{align*}
            K_{c}=&\frac{[\ce{FeSCN^{2+}}]}{[\ce{Fe^{3+}}][\ce{SCN-}]} \\[2mm]
            K_{c}=&\frac{\SI{2.5861246724e-5}{\mole\of{\ce{FeSCN^{2+}}}}}{\SI{9.74138753276d-4}{\mole\per\liter\of{\ce{Fe(NO3)3}}}\cdot\SI{1.74138753276d-4}{\mole\per\liter\of{\ce{KSCN}}}} \\[2mm]
            K_{c}=&\num{152.452027982107} \\[2mm]
            \approx&\num{152}
        \end{align*}
        So now we just need to find the average \(K_{c}\).
        \begin{gather*}
            \frac{K_{c1}+K_{c2}+K_{c3}+K_{c4}+K_{c5}}{5} \\[2mm]
            \frac{\splitfrac{\num{152.452027982107}+\num{148.658755129718}+\num{163.116644627126}}{+\num{163.116644627126}+\num{162.643119089258}}}{5} \\[2mm]
            K{c}^{\text{avg}}=\num{159.286179047239} \\[2mm]
            K{c}^{\text{avg}}=\num{159} \\[2mm]
        \end{gather*}
    \item[Calculating \(K_{c}\) for \ce{Fe(SCN)2+}]
        \begin{align*}
            \intertext{Mixture absorbances (mix)}
            \text{Mixture 1}& &A=.094 \\
            \text{Mixture 2}& &B=.180 \\
            \text{Mixture 3}& &C=.285 \\
            \text{Mixture 4}& &D=.383 \\
            \text{Mixture 5}& &E=.454 \\
            \intertext{Molarity of \ce{Fe(SCN)2+}.}
            f=&\frac{\SI{2.75119646d-4}{\liter\per\mole}}{2} (\iota) &[\text{mix}] \\[2mm] 
            \intertext{Moles of \ce{Fe(SCN)2+}.}
            g=&f\cdot\SI{.01}{\liter} &[\text{mix}] \\[2mm]
            \intertext{Equilibrium moles of \ce{Fe^{3+}}.}
            h=&\iota - g[\text{mix}] &[\text{Inital }\ce{Fe^{3+}}\si{\mole}] \\[2mm]
            \intertext{Equilibrium moles of \ce{SCN-}.}
            j=&\iota - 2\left(g[\text{mix}]\right) &[\text{Inital }\ce{SCN-}\si{\mole}] \\[2mm]
            \intertext{Mixture 1 \(K_{c}\).}
            K_{c1}=&\frac{f}{(h[\SI{1.00e-5}{\mole\of{\ce{Fe^{3+}}}}] \cdot j[\SI{2.00e-6}{\mole\of{\ce{SCN-}}}]^{2})100} &[A] \\[2mm]
            K_{c1}=&\num{431997.190002950}
        \end{align*}
        And finally \(K_{c}^{\text{avg}}\).
        \begin{gather*}
            \frac{K_{c1}+K_{c2}+K_{c3}+K_{c4}+K_{c5}}{5} \\[2mm]
            \frac{\splitfrac{\num{431997.190002950}+\num{206695.145466870}+\num{149984.202188881}}{+\num{115263.052375921}+\num{86725.8459275697}}}{5} \\[2mm]
            K_{c}^{\text{avg}}=\num{198133.087192438}
        \end{gather*}
    \item[1.] \hfill \\
        Well the closest to constant was equation 1 \ce{Fe^{3+} + SCN- <--> FeSCN^{2+}}. Because the calculated equilibrium constants for equation 2 were so different, I think it is safe to say that the thiocyanatoiron ion does not have two thiocyanates. Therefore the correct formula for this reaction is \ce{FeSCN^{2+}}.Because of the nature of a chemical equilibrium, combined with the fact that temperature deviations are at a minimum, this experiment should lead us to a ``somewhat'' constant \(K_{c}\). If the second equation was the correct one it would imply either, that the system had not yet reached equilibrium, or that this is not in fact a reversible reaction and that it is just moving very slowly. However I would say that, due to the fact that this experiment was designed to explore the concept of chemical equilibrium, it is likely that the first equation is the correct one, and that any deviations I found in the equilibrium constant are due to non ideal behavior of the component chemicals. From what I can tell we would likely have gotten closer results with the thermodynamic equilibrium constant.
    \item[2.] \hfill \\
        While I'm still trying to wrap my head around the differences between the mass action expression and the experimentally determined rate law I think I have gotten it due to this experiment (and the subsequent report). While a reaction rate law can have intermediate steps as well as orders other than the coefficient of the balanced chemical equation, the two rate laws of a reversible reaction when put into the mass action form will cancel each other out until all that is left is the differential rate law form of an elementary reaction with concentrations raised to the power of their coefficients in the balanced chemical equation. I learned that when the forward and reverse reaction rates of a reversible, single step,  reaction are equal, the ratio of to two reaction constants will be the equilibrium constant. So it would seem that if you knew the experimental rate laws for both forward and reverse reactions finding the equilibrium constant would be easy. I'm not sure any of this is important since the important thing is that the mass action expression will equal a constant for any reversible reaction at equilibrium. I learned that equilibrium calculations involving multiple very small numbers is exceedingly difficult at \(2:00\) in the morning. I think most importantly this experiment got me thinking about equilibrium in general, from a more metaphysical viewpoint, it would seem to me that all things, science, social, monetary, philosophical, tend towards equilibrium, and the concept itself is rather simple. The difficulty comes in finding an expression that properly describes the system and from that extrapolating a differential expression to determine the rate of change. From a simple chemical equation it is (relatively) easy to determine if our calculations are correct but as things become more complicated and our ability to measure concentration is eroded predicting when an equilibrium will be reached or even whether or not it's likely is exponentially more difficult.

\end{description}
\printbibliography
\end{document}
