\documentclass[12pt,oneside,letterpaper]{article}
\usepackage{rotating}
\usepackage{rotfloat}
\usepackage{mathtools}
\usepackage{amssymb}
\usepackage{siunitx}
\usepackage{mhchem}
\sisetup{per-mode  = reciprocal}%
\sisetup{qualifier-mode = subscript}%
\usepackage{hyperref}
\usepackage{tkz-berge}
\usepackage{color}
\usepackage{xfrac}
\usepackage{graphicx}
\usepackage{chemfig}
\usepackage[style = chem-acs]{biblatex}
\addbibresource{references.bib}
\setboolean{@twoside}{false}
%define identity function
\newcommand{\id}
{\big\iota}
% New definition of square root:
% it renames \sqrt as \oldsqrt
\usepackage{letltxmacro}
\makeatletter
\let\oldr@@t\r@@t
\def\r@@t#1#2{%
\setbox0=\hbox{$\oldr@@t#1{#2\,}$}\dimen0=\ht0
\advance\dimen0-0.2\ht0
\setbox2=\hbox{\vrule height\ht0 depth -\dimen0}%
{\box0\lower0.4pt\box2}}
\LetLtxMacro{\oldsqrt}{\sqrt}
\renewcommand*{\sqrt}[2][\ ]{\oldsqrt[#1]{#2} }
\makeatother
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\everymath{\displaystyle}
\title{\textit{Titration of a Unknown Diprotic Acid}}
\author{Caspian Baskin}

\begin{document}

\maketitle
\begin{description}
    \item[1.] \hfill \\
        As the acid closest to a listed molecular weight, maleic acid \ce{H2C4H4O5}, is the one I believe we were titrating. I chose this acid because if it were any of the others on the list, the difference between our experimental data and the accepted data would be far too great.
    \item[2.] \hfill \\
        Well it would seem from my data that the second derivative had the best resolution for predicting the molar mass, however they were all pretty far from being correct. It seems that the second ionization had a better prediction for molar mass but I'm not entirely sure why.
    \item[3.] \hfill \\
        \setangleincrement{30}\chemname{\chemfig{{\color{blue}H}-[1]\lewis{3:1:,O}-[11](=[9]\lewis{5:7:,O})-[1](-[3]H)=[11](-[1]H)-[9](>:[10]\lewis{4:7:,O}-[0]|{\textcolor{blue}H})=[7]\lewis{4:6:,O}}}{Maleic Acid}
    \item[4.] \hfill \\
        Well this is an easy one, first I have to calculate the amount of hydronium left after the larger amount of hydronium (completely ionized from the \ce{HCl}) reacts with the \ce{OH-} (completely ionized from the \ce{NaOH}). Then calculate the pH.
        \begin{align*}
            [\ce{HCL}]=&\SI{.250}{\mole\per\liter} \\
            [\ce{NaOH}]=&\SI{.125}{\mole\per\liter} \\
            \shortintertext{so,}
            [\ce{H+}]=&\SI{.250}{\mole\per\liter\of{\text{initial}}} \\
            [\ce{OH-}]=&\SI{.125}{\mole\per\liter\of{\text{initial}}} \\
            \shortintertext{so we have,}
            \frac{\SI{.250}{\mole\per\liter\of{\ce{H+}}}\left(\SI{.030}{\liter}\right)}{\SI{.060}{\liter\of{\text{solution}}}}=&\SI{.125}{\mole\per\liter\of{\ce{H+}}} \\
            \shortintertext{and,}
            \frac{\SI{.125}{\mole\per\liter\of{\text{OH-}}}\left(\SI{.030}{\liter}\right)}{\SI{.060}{\liter\of{\text{solution}}}}=&\SI{0.0630}{\mole\per\liter\of{\ce{OH-}}} \\
            \shortintertext{and the reaction equation is,}
            \ce{HCl(aq) + NaOH(aq) &-> NaCL(aq) + H2O(\ell)} \\
            \shortintertext{so it's a limiting agent problem, and since the reaction is essentially instantaneus and complete,}
            \SI{.125}{\mole\per\liter\of{\ce{H+}}} - \SI{.0625}{\mole\per\liter\of{\ce{OH-}}}=&\SI{.0625}{\mole\per\liter\of{\ce{H+}}} \\
            \shortintertext{so we just need to find the pH,}
            \frac{-\ln (\SI{.0625}{\mole\per\liter\of{\ce{H+}}})}{\ln (10)}=&\num{1.20411998265592} \\
            pH\approx&\num{1.204}
        \end{align*}
    \item[5.] \hfill \\
        Since this appears to be a buffer solution I will use the Henderson-Hasselbalch equation to approximate the pH.
        \begin{align*}
            pH=&pK_{a} + \text{log}\left(\frac{[\ce{AcO-}]}{[\ce{HAcO}]}\right) \\
            \shortintertext{for the equation,}
            \ce{HAcO(s) <-->& AcO-(aq) + H+(aq)} \\
            K_{a}=&\num{1.8e-5}\\
            pK_{a}=&-\text{log}\left(\num{1.8e-5}\right) \\
            pK_{a}=&\num{4.74472749489669} \\
            \approx&\num{4.74} \\
            \shortintertext{because acetic acid is a weak acid, and sodium acetate dissolves readily we can use,}
            pH=&\num{4.74}+\frac{\left[\SI{.50}{\mole\per\liter\of{\ce{AcO-}}}\right]}{\left[\SI{.75}{\mole\per\liter\of{\ce{HAcO}}}\right]} \\
            pH=&\num{4.56863623584101} \\
            \approx&\num{4.57}
        \end{align*}
    \item[6.] \hfill \\
        Since \ce{NaOH} is a strong base and ionizes almost completely we can calculate the amount of \ce{OH-} ions and subtract that from the mass action expression, and then calculate equilibrium concentrations from the acid dissociation constant.
        \begin{align*}
            \frac{\SI{.50}{\mole\per\liter\of{\ce{NaOH}}}\left(\SI{.015}{\liter}\right)}{\SI{.045}{\liter\of{\text{solution}}}}=&\SI{0.166666666666667}{\mole\per\liter\of{\ce{NaOH}}} \\
            \approx&\SI{.17}{\mole\per\liter\of{\ce{NaOH}}} \\
            \frac{\SI{.50}{\mole\per\liter\of{\ce{C6H5COOH}}}\left(\SI{.030}{\liter}\right)}{\SI{.045}{\liter\of{\text{solution}}}}=&\SI{0.333333333333333}{\mole\per\liter\of{\ce{C6H5COOH}}} \\
            \approx&\SI{.33}{\mole\per\liter\of{\ce{C6H5COOH}}} \\
            \shortintertext{the chemical equation is,}
            \ce{C6H5COOH <--> C6H5COO- + H+} \\
            K_{a}=&\num{6.5e-5} \\
            \shortintertext{and of course,}
            \ce{OH- + H+ <--> H2O}
            \shortintertext{because the concentration of \ce{OH-} is so high we will assume this reaction is complete, so,}
            \num{6.5e-5}=&\frac{\iota\left(\iota-\SI{.17}{\mole}\right)}{\SI{.33}{\mole}-\iota}   &[\ce{C6H5COO-}] \\
            \iota^{2}-\num{.17}\iota=&\num{6.5e-5}\left(\num{.33}-\iota\right) &[\ce{C6H5COO-}] \\
            \iota^{2}-\num{.169935}\iota=&\num{2.145e-5} &[\ce{C6H5COO-}] \\
            \left(\iota-\frac{\num{.169935}}{2}\right)^{2}=&\num{2.145e-5}+\left(\frac{\num{-.169935}}{2}\right)^{2} &[\ce{C6H5COO-}] \\
            \iota-\num{0.0849675}=&\sqrt{\num{0.00724092605625}} &[\ce{C6H5COO-}] \\
            \iota=&\pm \num{0.0850936311144965}+\num{0.0849675} &[\ce{C6H5COO-}] \\
            \iota=&\num{0.170061131114496} &[\ce{C6H5COO-}] \\
            \approx&\SI{.17}{\mole\per\liter\of{\ce{C6H5COO-}}} \\
            \shortintertext{so, \ce{H+} would equal the concentration of \ce{C6H5COO-} if there was no \ce{NaOH}, so we need to subtract,}
            [\ce{H+}]-[\ce{OH-}]=&\SI{0.00339446444782945}{\mole\per\liter\of{\ce{H+}}} \\
            \shortintertext{so,}
            pH=&-\text{log}\left(\num{0.00339446444782945}\right) \\
            pH\approx&\num{2.47}
        \end{align*}
        \item[7.] \hfill \\
        I'll do the same for this one although I'll try to clean up the calculations a little bit to speed things up. I'll define the function, rearrange it for a solution, and then enter each consecutive step. Because the \ce{NaOH} is a strong base I'll assume that it ionises completely, and because of the water ``self-ionization'' equilibrium \ce{H2O <--> H+ + OH-} and that fact that we will be starting with a comparatively large amount of \ce{H+} we will assume that when \ce{OH-} is added it combines completely with \ce{H+} to make water and bring that equilibrium closer to balanced.
        \begin{align*}
            \shortintertext{the mass action expression for this problem is,}
            K_{a}=&\frac{[\ce{ClO-}]\left([\ce{H+}]-[\ce{OH-}]\right)}{[\ce{HClO}]} \\
            \shortintertext{the first piece of the composite function will define the amount of \SI{.100}{M} \ce{NaOH} solution added}
            h=&\id\cdot\SI{.010}{\liter\of{\SI{.100}{M}\ce{NaOH}\text{solution}}} &{1,2,3,4} \\
            \shortintertext{the second part of the composite function will define the initial concentration of \ce{HClO},}
            g[h]=&\frac{\SI{.100}{\mole\per\liter\of{\ce{HClO}}}\left(\SI{.040}{\liter}\right)}{\SI{.040}{\liter\of{\text{solution}}}+h[\id]}  \\
            \shortintertext{now I'll define the concentration of \ce{NaOH} in the equillibrium solution,}
            j[h]=&\frac{\SI{.100}{\mole\per\liter\of{\ce{NaOH}}}\left(h[\id]\right)}{\SI{.040}{\liter\of{\text{solution}}}+h[\id]} \\
        \end{align*}
        \begin{align*}
            \shortintertext{so now that we've established our initial concentrations we should figure out what the limiting reagent will be, so lets figure out what \ce{H+} would be, disregarding \ce{OH-} ions, but including the amount of added solution,}
            K_{a}=&\num{3.0e-8} \\
            \shortintertext{and from the mass action expression we have,}
            \num{3.0e-8}=&\frac{\id\left(\id\right)}{g[h]-\id} &[\Delta \ce{H+}] \\
            \id^{2}=&\num{3.0e-8}\left(g[h]-\id\right) &[\Delta \ce{H+}] \\
            \id^2 + \num{3.0e-8}\id =& \num{3.0e-8}g[h] &[\Delta \ce{H+}] \\
            \left(\id + \frac{\num{3.0e-8}}{2}\right)^{2}=&\num{3.0e-8}g[h] + \left(\frac{\num{3.0e-8}}{2}\right)^{2} &[\Delta \ce{H+}] \\
            \id + \frac{\num{3.0e-8}}{2} =& \sqrt{\num{3.0e-8}g[h] + \left(\frac{\num{3.0e-8}}{2}\right)^{2}} &[\Delta \ce{H+}] \\
            \id = \sqrt{\num{3.0e-8}g[h] + \left(\frac{\num{3.0e-8}}{2}\right)^{2}}& -\frac{\num{3.0e-8}}{2} &[\Delta \ce{H+}] \\
            \shortintertext{so,}
            f[h]=\sqrt{\num{3.0e-8}g[h] + \left(\frac{\num{3.0e-8}}{2}\right)^{2}}& -\frac{\num{3.0e-8}}{2} \\
            \shortintertext{so the \Delta [\ce{H+}] if the added \ce{NaOH} was just water would be,}
            f[1]=&\num{0.0000489747971520601} \\
            f[2]=&\num{0.0000447063620655722} \\
            f[3]=&\num{0.0000413889362776743} \\
            f[4]=&\num{0.0000387148363668116} \\
            \shortintertext{and the concentrations of \ce{OH-} ions, if the \ce{HClO} was just water would be,}
            j[1]=&\num{0.00243902439024390} \\
            j[2]=&\num{0.00476190476190476} \\
            j[3]=&\num{0.00697674418604651} \\
            j[4]=&\num{0.00909090909090909} \\
            \shortintertext{so, since [\ce{OH-}] is so much larger than [\ce{H+}], we can assume that all the \ce{H+} is used up to the point that the solution is saturated with \ce{ClO-} and no more \ce{HClO} dissociates. So if we disregard [\ce{H+}] in the mass action and find [\ce{ClO-}] we will know the amount of \ce{H+} that the solution can generate (which will all be used up by the excess \ce{OH-}).}
            \num{3.0e-8}=&\frac{\id}{g[h]-\id} &[\Delta \ce{ClO-}] \\
            \id=&\num{3.0e-8}\left(g[h]-\id\right) &[\Delta \ce{ClO-}] \\
            \id\left(1+\num{3.0e-8}\right)=&\num{3.0e-8}g[h] &[\Delta \ce{ClO-}] \\
            \id=&\frac{\num{3.0e-8}g[h]}{1+\num{3.0e-8}} &[\Delta \ce{ClO-}] \\
            \shortintertext{so,}
            f[h]=&\frac{\num{3.0e-8}g[h]}{1+\num{3.0e-8}} \\
            \shortintertext{so now we can finally define that last part,}
            pH[h]=&14+\text{log}\left(j[h]-f[h]\right) \\
        \end{align*}
        \newpage
        So now that we've done all this ridiculous preparation, we can finally answer the question!
        \begin{align*}
            pH=&\num{11.3872157159343} &[1] \\
            \approx&\num{11.387} \\
            pH=&\num{11.6777805228624} &[2] \\
            \approx&\num{11.678} \\
            pH=&\num{11.8436526924277} &[3] \\
            \approx&\num{11.844} \\
            pH=&\num{11.9586072431832} &[4] \\
            \approx&\num{11.959}
        \end{align*}
        Well it's plain to see that this did not exactly speed up the process, but it sure was fun.

\printbibliography
\end{document}

